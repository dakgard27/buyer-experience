---
title: Support Portal
description: Support Portal
side_menu:
  links:
  - text: First time reaching support?
    href: "#first-time-reaching-support"
    data_ga_name: First time reaching support?
    data_ga_location: header
    children:
    - text: Setting up an account on support.gitlab.com
      href: "#setting-up-an-account-on-supportgitlabcom"
  - text: "Having trouble with the Support Portal?"
    href: "#having-trouble-with-the-support-portal"
    children:
    - text: "Language Support"
      href: "#language-support"
components:
- name: call-to-action
  data:
    title: Support Portal
    centered_by_default: true
    hide_title_image: true
- name: copy
  data:
    block:
    - hide_horizontal_rule: true
      no_margin_bottom: true
      text: |
        ## First time reaching support? {#first-time-reaching-support}

        If you just purchased a license, there's a few things to do to make sure the folks maintaining your GitLab get the smoothest support experience possible.

        1. [Set up an account](#setting-up-an-account-on-support-gitlab-com) on [support.gitlab.com](https://support.gitlab.com/hc/en-us/requests/new){data-ga-name="support request" data-ga-location="body"}
        1. Create your first ticket [to register your company's authorized support contacts](/support/managing-support-contacts.html#getting-set-up).
           - (Optional) Set up a [shared organization](/support/managing-support-contacts.html#shared-organizations) so that your authorized support contacts can see each others tickets.
        1. Just in case someone is left out, circulate [instructions on proving your support entitlement](/support/managing-support-contacts.html#proving-your-support-entitlement) within your company.
        1. Learn about [how to work effectively in support tickets](/support/#working-effectively-in-support-tickets).
        1. Familiarize yourself with our [Statement of Support](/support/statement-of-support.html) to understand the scope of what to expect in your interactions.
        1. Take a look at [what's included in Priority Support](/support/#priority-support) and the [effect choosing a support region has on tickets](https://about.gitlab.com/support/#effect-on-support-hours-if-a-preferred-region-for-support-is-chosen) to understand the time frame in which you can expect a first response.

        ### Setting up an account on support.gitlab.com {#setting-up-an-account-on-supportgitlabcom}
        Account setup can happen one of two ways:

        1.  Go to [support.gitlab.com](https://support.gitlab.com/hc/en-us/requests/new){data-ga-name="support request" data-ga-location="body"} and submit a new request. An account and password will be created for you. You will need to request a password reset and setup a new password before you can sign in.
        2.  Click on [Sign Up](https://gitlab.zendesk.com/auth/v2/login/registration?auth_origin=3252896%2Ctrue%2Ctrue&brand_id=3252896&return_to=https%3A%2F%2Fsupport.gitlab.com%2Fhc%2Fen-us&theme=hc){data-ga-name="support sign up" data-ga-location="body"} to create a new account using your company email address.

        You can keep track of all of your tickets and their current status using the GitLab Support Portal! We recommend using the Support Portal for a superior experience managing your tickets. To learn more about using the Support Portal, watch [this video on using **Zendesk as an end user**](https://www.youtube.com/watch?v=NQzkGD7nIqQ).

        ## Having trouble with the Support Portal? {#having-trouble-with-the-support-portal}

        Occasionally, you may find the [Support Portal](https://support.gitlab.com){data-ga-name="support portal" data-ga-location="first time"} not acting as expected. This is often caused by the user’s setup. When encountering this, the recommended course of action is:

        1. Ensure your browser is allowing third party cookies. These are often vital for the system to work. A general list to allow would be:
           * `[*.]zendesk.com`
           * `[*.]zdassets.com`
           * `[*.]gitlab.com`
        1. Disable all plugins/extensions/addons on the browser.
        1. Disable any themes on the browser.
        1. Clear all cookies and cache on the browser.
        1. Try logging in again to the the Support Portal.
        1. If you are still having issues, write down the browser’s version, type, distro, and other identifying information.
        1. Generate a HAR file (process will vary from browser to browser) and send this to support. If you are unable to create a ticket, then communicate with your Technical Account Manager and/or Account Manager. The next best place to send HAR file and browser information is via a GitLab.com issue.

        ### Language Support {#language-support}

        Ticket support is available in the following languages:
          - Chinese
          - English
          - French
          - German
          - Japanese
          - Korean
          - Portuguese
          - Spanish

        While we do not currently provide translated interfaces for our [Support Portal](https://support.gitlab.com), you can write in your preferred language and we will respond accordingly.

        Should you be offered a call, only English is available.

        NOTE:
        Any attached media used for ticket resolution must be sent in English.
